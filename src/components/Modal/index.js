import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';

function Modal({ isOpen, onClose, children }) {
  return ReactDOM.createPortal(
    <div className={`modal ${isOpen ? 'open' : ''}`}>
      <div className="modal-content">
        <div className="modal-content-header">
          <button onClick={onClose}>X</button>
        </div>
        <div className="modal-content-body">
          {children}
        </div>
      </div>
    </div>,
    document.getElementById('modalRoot')
  );
}

export default Modal;
