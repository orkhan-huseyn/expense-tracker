import React from 'react';
import './styles.css';

function Summary({ total }) {
  return (
    <div className="summary-container">
      <div className="summary">
        <h1>{total} ₼</h1>
        <small>Mövcud balansınız</small>
      </div>
    </div>
  );
}

export default Summary;
