import React, { useState } from 'react';

function Form({ onSubmit }) {
  const [value, setValue] = useState(0);
  const [type, setType] = useState('income');

  function handleCheckboxChange() {
    if (type === 'income')
      setType('expense');
    else
      setType('income');
  }

  function handleFormSubmit(event) {
    event.preventDefault();
    onSubmit({ value, type });
    setValue('');
  }

  return (
    <form onSubmit={handleFormSubmit} className="form">
      <input
        type="number"
        placeholder="Add expense or income"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
      <input
        type="checkbox"
        checked={type === 'income'}
        onChange={handleCheckboxChange}
      />
    </form>
  );
}

export default Form;
