import React, { useState } from 'react';
import Summary from './components/Summary';
import History from './components/History';
import Form from './components/Form';
import Modal from './components/Modal';

function App() {
  const [total, setTotal] = useState(0);
  const [history, setHistory] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);

  function handleFormSubmit({ value, type }) {
    if (type === 'income') {
      setTotal(total + Number(value));
    } else {
      setTotal(total - Number(value));
    }

    const historyItem = { value, type };
    const newHistory = [...history, historyItem];
    setHistory(newHistory);
    closeModal();
  }

  function openModal() {
    setModalOpen(true);
  }

  function closeModal() {
    setModalOpen(false);
  }

  return (
    <>
      <Modal
        isOpen={isModalOpen}
        onClose={closeModal}
      >
        <h1>Hello, World!</h1>
      </Modal>

      <Summary total={total} hello="asdsd"/>

      <History history={history} />

      <button onClick={openModal} className="floating-button">
        <svg viewBox="0 0 455 455">
          <polygon fill="#ffffff" points="455,212.5 242.5,212.5 242.5,0 212.5,0 212.5,212.5 0,212.5 0,242.5 212.5,242.5 212.5,455 242.5,455 242.5,242.5
            455,242.5 "/>
        </svg>
      </button>
    </>
  );
}

export default App;
